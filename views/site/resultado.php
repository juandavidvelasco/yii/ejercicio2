<?php
use yii\grid\GridView;
?>

<div class="jumbotron">
    <h2><?=$titulo ?></h2>
    <p><?= $enunciado?></p>
    <div class="well">
        <?= $sql ?>
    </div>
</div>
<?=GridView::widget([
    'dataProvider' => $resultado,
    'columns' => $campos
]); ?>


