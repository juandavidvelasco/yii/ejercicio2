<?php
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'Consultas de Seleccion 2';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">Consultas de Seleccion</h1>

    </div>

    <div class="body-content">

        <div class="row">
            
            <!--Primera consulta-->
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 1</h3>
                        <p> Número de ciclistas que hay </p> 
                        <p>
                            <?= Html::a('Active Record',['site/consulta1'],['class'=> 'btn btn-primary'])?>
                            <?= Html::a('Dao',['site/consulta1a'],['class' => 'btn btn-warning']) ?>
                        </p>
                    </div>
                </div>
            </div>
            <!--Fin primera consulta-->
            
            <!--Consulta 2-->
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 2</h3>
                        <p>Número de ciclistas que hay del equipo Banesto </p> 
                        <p>
                            <?= Html::a('Active Record',['site/consulta2'],['class'=> 'btn btn-primary'])?>
                            <?= Html::a('Dao',['site/consulta2a'],['class' => 'btn btn-warning']) ?>
                        </p>
                    </div>
                </div>
            </div>
            <!--Fin Consulta 2-->
            
            <!--Consulta 3-->
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 3</h3>
                        <p>Edad media de los ciclistas</p> 
                        <p>
                            <?= Html::a('Active Record',['site/consulta3'],['class'=> 'btn btn-primary'])?>
                            <?= Html::a('Dao',['site/consulta3a'],['class' => 'btn btn-warning']) ?>
                        </p>
                    </div>
                </div>
            </div>
            <!--Fin consulta 3-->
            
            <!--consulta 4-->
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 4</h3>
                        <p>La edad media de los del equipo Banesto</p> 
                        <p>
                            <?= Html::a('Active Record',['site/consulta4'],['class'=> 'btn btn-primary'])?>
                            <?= Html::a('Dao',['site/consulta4a'],['class' => 'btn btn-warning']) ?>
                        </p>
                    </div>
                </div>
            </div>
            <!--Fin consulta 4-->
            
            <!--consulta 5-->
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 5</h3>
                        <p>Edad media de los ciclistas</p> 
                        <p>
                            <?= Html::a('Active Record',['site/consulta5'],['class'=> 'btn btn-primary'])?>
                            <?= Html::a('DAO',['site/consulta5a'],['class'=> 'btn btn-warning']) ?>
                        </p>
                    </div>
                </div>
            </div>
            <!--fin consulta 5-->
            
            <!--consulta 6-->
             <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 6</h3>
                        <p>El número de ciclistas por equipo</p> 
                        <p>
                            <?= Html::a('Active Record',['site/consulta6'],['class'=> 'btn btn-primary'])?>
                            <?= Html::a('DAO',['site/consulta6a'],['class'=> 'btn btn-warning']) ?>
                        </p>
                    </div>
                </div>
            </div>
            <!--Fin consulta 6-->
            
            <!--cosulta 7-->
             <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 7</h3>
                        <p>El número total de puertos</p> 
                        <p>
                            <?= Html::a('Active Record',['site/consulta7'],['class'=> 'btn btn-primary'])?>
                            <?= Html::a('DAO',['site/consulta7a'],['class'=> 'btn btn-warning']) ?>
                        </p>
                    </div>
                </div>
            </div>
            <!--FIN CONSULTA 7-->
            
            <!--CONSULTA 8-->
             <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 8</h3>
                        <p>El número total de puertos mayores de 1500</p> 
                        <p>
                            <?= Html::a('Active Record',['site/consulta8'],['class'=> 'btn btn-primary'])?>
                            <?= Html::a('DAO',['site/consulta8a'],['class'=> 'btn btn-warning']) ?>
                        </p>
                    </div>
                </div>
            </div>
            <!--FIN CONSULTA 8-->
            
            <!--CONSULTA 9-->
             <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 9</h3>
                        <p>Listar el nombre de los equipos que tengan más de 4 ciclistas</p> 
                        <p>
                            <?= Html::a('Active Record',['site/consulta9'],['class'=> 'btn btn-primary'])?>
                            <?= Html::a('DAO',['site/consulta9a'],['class'=> 'btn btn-warning']) ?>
                        </p>
                    </div>
                </div>
            </div>
            <!--FIN CONSULTA 9-->
            
            <!--CONSULTA 10-->
             <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 10</h3>
                        <p>Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32</p> 
                        <p>
                            <?= Html::a('Active Record',['site/consulta10'],['class'=> 'btn btn-primary'])?>
                            <?= Html::a('DAO',['site/consulta10a'],['class'=> 'btn btn-warning']) ?>
                        </p>
                    </div>
                </div>
            </div>
            <!--FIN CONSULTA 10-->
            
            <!--CONSULTA 11-->
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 11</h3>
                        <p> Indícame el número de etapas que ha ganado cada uno de los ciclistas </p> 
                        <p>
                            <?= Html::a('Active Record',['site/consulta11'],['class'=> 'btn btn-primary'])?>
                            <?= Html::a('Dao',['site/consulta11a'],['class' => 'btn btn-warning']) ?>
                        </p>
                    </div>
                </div>
            </div>
            <!--FIN CONSULTA 11-->
            
            <!--CONSULTA 12-->
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 12</h3>
                        <p> Indícame el dorsal de los ciclistas que hayan ganado más de una etapa </p> 
                        <p>
                            <?= Html::a('Active Record',['site/consulta12'],['class'=> 'btn btn-primary'])?>
                            <?= Html::a('Dao',['site/consulta12a'],['class' => 'btn btn-warning']) ?>
                        </p>
                    </div>
                </div>
            </div>
            <!--FIN CONSULTA 12-->
        </div>

    </div>
</div>
